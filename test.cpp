#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "funzioni.h"

TEST_CASE("sequenze di due operazioni ")
{
    int a = 5;

    SECTION("incremento")
    {
        inc(&a);

        REQUIRE(a == 6);

        SECTION("incremento")
        {
            inc(&a);
            REQUIRE(a == 7);
        }

        SECTION("decremento")
        {

            dec(&a);
            REQUIRE(a == 5);
        }
    }

    SECTION("decremento")
    {
        dec(&a);

        REQUIRE(a == 4);
    }
}

/*

TEST_CASE ("sequenze di due operazioni decremento ") {
    int a = 5;

    SECTION ("decremento") {
dec (&a);


    REQUIRE( a ==4);

    SECTION ("incremento") {
    inc (&a);
    REQUIRE( a ==5);
    }

SECTION ("decremento") {

    dec (&a);
    REQUIRE( a ==3);
}
    }
}
*/

/*
TEST_CASE ("incremento poi decremento ") {
    int a = 5;
inc (&a);


    REQUIRE( a ==6);
    dec (&a);
    REQUIRE( a ==5);

}
*/